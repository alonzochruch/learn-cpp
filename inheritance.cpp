/*
Base:
  class derived-class: access-specifier base-class
 */

/*
Multiple Inheritance:
  class derived-class: access baseA, access baseB....
 */

/*
Public mode: If we derive a sub class from a public base class. Then the public member of the base class will become public in the derived class and protected members of the base class will become protected in derived class.
Protected mode: If we derive a sub class from a Protected base class. Then both public member and protected members of the base class will become protected in derived class.
Private mode: If we derive a sub class from a Private base class. Then both public member and protected members of the base class will become Private in derived class. 
 */

class A
{
public:
    int x;
protected:
    int y;
private:
    int z;
};

class B : public A
{
    // x is public
    // y is protected
    // z is not accessible from B
};
 
class C : protected A
{
    // x is protected
    // y is protected
    // z is not accessible from C
};
 
class D : private A    // 'private' is default for classes
{
    // x is private
    // y is private
    // z is not accessible from D
};

// End

/*
Avoiding ambiguity using scope resolution operator: 
  A a; 
  a.AnotherClass::Variable
 */

class AAA
{
public:
    int x;
};

class BBB : public AAA
{
public:
     int y;
};

class CCC: public AAA
{
public:
     int z;
};

class DDD : public BBB, public CCC
{
public:
    int xyz;
};

DDD dd;
dd.BBB::x = 100;
dd.CCC::x = 200;
dd.y = 300;
dd.z = 500;

/*
Avoiding ambiguity using virtual base class: 
 */

class AA
{
public:
    int x;
};

class BB : virtual public AA
{
public:
     int y;
};

class CC: virtual public AA
{
public:
     int z;
};

class DD : public BB, public CC
{
public:
    int xyz;
};

DD d;
dd.x = 10;
dd.y = 20;
dd.z = 30;
dd.xyz = 102030;

/*
Public Inheritance means all public members of the base class are accessible to the derived class
Private Inheritance means all members of the base class are private to the derived class
Protected Inheritance means all members of the base class are protected to the derived class.
 */

/*
Public Inheritance − When deriving a class from a public base class, public members of the base class become public members of the derived class and protected members of the base class become protected members of the derived class. A base class's private members are never accessible directly from a derived class, but can be accessed through calls to the public and protected members of the base class.

Protected Inheritance − When deriving from a protected base class, public and protected members of the base class become protected members of the derived class.

Private Inheritance − When deriving from a private base class, public and protected members of the base class become private members of the derived class.
 */