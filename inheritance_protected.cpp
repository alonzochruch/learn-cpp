#include <iostream>

class A
{
private:
    int m_y;

protected:
    int p_z;

public:
    int x;
    
    A() : x {0}, m_y {10000}, p_z {20000} {}
    
    int getX() const
    {
        return x;
    }
    
    int getY() const
    {
        return m_y;
    }
    
    int getZ() const
    {
        return p_z;    
    }
};

class B : protected A
{
public:
    int getX() const
    {
        return A::getX();
    }
    
    int getY() const
    {
        return A::getY();
    }
    
    int getZ() const
    {
        return A::getZ();
    }
};


int main()
{
    B b;
    
    std::cout << b.getX() << "\n";
    std::cout << b.getY() << "\n";
    std::cout << b.getZ() << "\n";
    
    return 0;
}